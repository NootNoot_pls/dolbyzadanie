import re
from os import popen
from random import randint

def extract_index():
    ctags = popen('ctags -x library.c')
    tags = ctags.read()
    lines = tags.splitlines()
    return lines

def extract_symbols():
    symbols = []
    lines = extract_index()

    for line in lines:
        symbols.append(re.split(r'\s{2,}',line)[0])

    return symbols

def extract_declarations():
    declarations = []
    ready = []
    lines = extract_index()

    for line in lines:
        declarations.append(re.split(r'\s{2,}',line)[3])

    for i in range (0,len(declarations)):
        declarations[i]=re.sub('[{}]', '', declarations[i])

    return declarations

def encrypt(word):
    eWord = ''
    for letter in word:
        random = randint(65,90)
        eWord = eWord + chr(random)
    return eWord

def encrypt_symbols():
    symbolDict = {}
    symbols = extract_symbols()

    for symbol in symbols:
        symbolDict[symbol] = encrypt(symbol)

    return symbolDict


symbols = encrypt_symbols()
declarations = extract_declarations()

with open('stirng.h','w') as header:

    for key, value in symbols.items():
        header.write ("#define %s %s\n"%(key, value))

    for declaration in declarations:
        header.write("%s;\n" % declaration)
header.closed

